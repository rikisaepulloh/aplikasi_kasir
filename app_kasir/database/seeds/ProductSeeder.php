<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $product = [
          ['name' => 'Sabun Batang',
          'cash_price' => 3000,
          'credit_price' => 3000
          ],
          ['name' => 'Mi Instan',
          'cash_price' => 2000,
          'credit_price' => 2000
          ],
          ['name' => 'Pensil',
          'cash_price' => 1000,
          'credit_price' => 1000
          ],
          ['name' => 'Kopi',
          'cash_price' => 1500,
          'credit_price' => 1500
          ],
          ['name' => 'Air Minum Galon',
          'cash_price' => 20000,
          'credit_price' => 20000
          ],
          ['name' => 'Sabun Batang',
          'cash_price' => 3000,
          'credit_price' => 3000
          ]
        ];
    }
}
