<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'           => 'Riki Saepulloh',
            'username'       => 'kasir',
            'password'       => '12345',
            'remember_token' => str_random(10),
        ]);
    }
}
